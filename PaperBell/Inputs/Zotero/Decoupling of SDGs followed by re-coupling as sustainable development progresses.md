---
zotero-key: JM3T7KEC
zt-attachments:
  - "1834"
title: Decoupling of SDGs followed by re-coupling as sustainable development progresses
citekey: wu2022b
tags:
  - paper
  - M
cate: 论文
concepts: []
read: 🤔精读
source: 错误：没有找到符合条件的标签。
authors:
  - Xutong Wu
  - Bojie Fu
  - Shuai Wang
  - Shuang Song
  - Yingjie Li
  - Zhenci Xu
  - Yongping Wei
  - Jianguo Liu
journal: Nature Sustainability
paper_date: 2022
date: 2024-04-30
important: true
---
# Decoupling of SDGs followed by re-coupling as sustainable development progresses

| Zotero | File | Journal |
| ---- | ---- | ---- |
| [Zotero](zotero://select/library/items/JM3T7KEC) | [attachment](<file:///Users/songshgeo/Zotero/storage/N92WR4ZH/Wu%20%E7%AD%89%E3%80%82%20-%202022%20-%20Decoupling%20of%20SDGs%20followed%20by%20re-coupling%20as%20sust.pdf>) | [Nature Sustainability](https://www.nature.com/articles/s41893-022-00868-x) |

> [!Abstract]
> 
> 

---
## Annotations



