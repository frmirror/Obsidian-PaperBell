<div align="center"><img src="https://firebasestorage.googleapis.com/v0/b/swimmio.appspot.com/o/repositories%2FZ2l0aHViJTNBJTNBUGFwZXJCZWxsJTNBJTNBU29uZ3NoR2Vv%2Fec47d268-80c8-4df5-bf62-1c37a2f43a3b.gif?alt=media&token=bb7fe1a8-5a44-4364-9032-fe20a563bad4" style="width:'100%'"/></div>

`PaperBell`: 用 Obsidian 优雅地积累学术笔记。

👋 大家好，欢迎下载和使用 `PaperBell` 示例仓库。制作这个仓库的初衷，是将自己过去两年间使用 Obsidian 管理学术生活的思考整理分享给大家。

1. 🤔️ 这个仓库是根据自己的需求建立的，请先判断`PaperBell`[是否适用于你](PaperBell/Outputs/是否适用于你.md)
2. 🙋 如果你确定这个仓库适用于你，先欢迎来熟悉下本仓库的[设计理念](PaperBell/Outputs/设计理念.md)
3. ✍️ 我们提倡将笔记分为[输入资料](PaperBell/Outputs/设计理念.md#输入资料)、[日常记录](PaperBell/Outputs/设计理念.md#日常记录)、和[输出笔记](PaperBell/Outputs/设计理念.md#输出笔记)三大类，你可以阅读以下利用 `PaperBell` 做笔记的方法：
	- 输入资料：[使用PaperBell阅读与整理资料](PaperBell/Outputs/使用PaperBell阅读与整理资料.md)
	- 日常记录：[使用PaperBell进行日常记录](PaperBell/Outputs/使用PaperBell进行日常记录.md)
	- 输出笔记：[使用PaperBell整理输出笔记](PaperBell/Outputs/使用PaperBell整理输出笔记.md)
4. 🗺️ 最后，如果你喜欢`PaperBell`，欢迎听听我[关于科研生活的探讨](PaperBell/Outputs/关于科研生活的探讨.md)

## 核心特性

`PaperBell` 将自动关联与项目有关的所有笔记。

![追踪项目](https://songshgeo-picgo-1302043007.cos.ap-beijing.myqcloud.com/uPic/%E8%BF%BD%E8%B8%AA%E9%A1%B9%E7%9B%AE.gif)

`PaperBell` 帮你关联所有一闪而过的念头与相关科学概念。

![概念-想法的链接](https://songshgeo-picgo-1302043007.cos.ap-beijing.myqcloud.com/uPic/%E6%A6%82%E5%BF%B5-%E6%83%B3%E6%B3%95%E7%9A%84%E9%93%BE%E6%8E%A5.gif)

`PaperBell` 帮你关联收藏的学者和读过的他们的论文。

![关联论文与学者](https://songshgeo-picgo-1302043007.cos.ap-beijing.myqcloud.com/uPic/%E5%85%B3%E8%81%94%E8%AE%BA%E6%96%87%E4%B8%8E%E5%AD%A6%E8%80%85.gif)

## 碎碎念

1. 本库基于我个人研究经验的总结，不一定适用于所有学科，更不一定适用于所有人。
2. 我不指望从开源分享中获取经济利益，希望**任何人不要包装这个库去赚钱**，你们忍心利用信息差从本就贫穷的中国研究生手里赚💰吗？
3. `PaperBell`专注于让 Obsidian 服务于学术研究的需要。由于技术和时间有限，**本示例库不会花很多时间在外观上**。如有兴趣折腾仓库的外观，可以参考 [Rainbell129 的 RainBell 示例仓库](https://github.com/Rainbell129/Obsidian-Homepage) 和 [Cuman的Blue Topaz example vault](https://github.com/cumany/Blue-topaz-examples)  这两个仓库已经丰富的文档，本仓库完全在两者的基础上二度开发。
4. 最后奉上一个我自己做的 meme。学术做得好不好，并不取决于“器”，而是“道”。不过嘛，道可道非常道，切莫本末倒置就好。

## 关于作者

开源项目的维护需要动力，离不开各位用户的支持。

按照开源社区的江湖规矩，相对直接的经济支持当然是 “Buy me a coffee”：
<a href="https://www.buymeacoffee.com/USgxYspYW4" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/default-orange.png" alt="Buy Me A Coffee" height="41" width="174"></a>
但对还没有工作收入的学生党，我强烈反对在这种地方花钱。你完全可以[写一封邮件](mailto:songshgeo@gmail.com)告诉我，我的开源项目对你有帮助，这对我来说就是极大的鼓励了。包括`PaperBell`，我还维护一个名为 [ABSESpy](https://github.com/ABSESpy/ABSESpy) 的专业开源软件项目，给我一个免费的 star 也是很感激的🥹。

或者，我每月都会更新一篇旅行随笔，也许你想为我的文章贡献一两个阅读量，可以关注我的[博客](https://www.songshgeo.com/)或[公众号](https://mp.weixin.qq.com/s/PYvT6zpf9WYnunlXN2x4YA)。或者也许更让我开心的是，告诉我你愿意在某天我经过你的家乡时，为我当半天导游？Then, let's keep in touch...

![个人banner2](https://songshgeo-picgo-1302043007.cos.ap-beijing.myqcloud.com/uPic/%E4%B8%AA%E4%BA%BAbanner2.png)

最后，我为`PaperBell`项目创建了一个微信交流社群，欢迎交流任何关于学术生活的感想，可关注我的公众号【隅地】发送关键字“Obsidian”或“PaperBell”获取最新群聊二维码。

任何开源项目相关问题欢迎邮件联系我 songshgeo[at]gmail[dot]com
